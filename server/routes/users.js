const express = require('express')
const users = express.Router()
const passport = require('passport')
const passportJWT = require('../config/passeport')(passport)
const jwt = require('jsonwebtoken')
const config = require('../config/index')

// Profile
users.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({
        user: req.user
    })
})

module.exports = users