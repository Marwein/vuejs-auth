const express = require('express')
const appli = express.Router()

appli.get('/register', (req, res) => {
    res.redirect('/#/register')
})

appli.get('/login', (req, res) => {
    res.redirect('/#/login')
})

module.exports = appli