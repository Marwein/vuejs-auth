var express = require('express');
var auth = express.Router();
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../config/index');
var User = require('../models/user');
var csrf = require('csurf');
var csrfProtection = csrf();
var app = require('express')();
var errors = require('../config/errors');
var uuid = require('uuid/v4');

app.use(csrfProtection);

// Registeration
auth.get('/register', function(req, res) {
    res.redirect('/#/auth/register');
})

auth.post('/register', function(req, res, next) {
    let newUser = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });

    User.create(newUser, function(err, user) {
        if (err) {
            res.status(errors.user.error.failedToRegister);
            res.json({ success: false, msg: 'Failed to register user', error: err });
        } else {
            res.status(errors.user.success.registered);
            res.json({ success: true, msg: 'User registered' });
        }
    });

});

// Forget Password
auth.get('/forgot', function(req, res, next) {
    res.redirect('/#/auth/forgot');
});

auth.post('/forgot', function(req, res, next) {
    var email = req.body.email;

    User.getUserBy('email', email, function(err, user) {
        if (err)
            throw err;
        if (!user) {
            res.status(errors.user.error.notFound);
            return res.json({
                suc: false,
                msg: 'User not found'
            });
        } else {
            console.log('envoi d\'email pour reset pass');
            res.status(errors.user.success.reset);
            return res.json({
                suc: true,
                msg: 'check your inbox mail to reset your password'
            });
        }
    });
});

// Authentication
auth.get('/login', csrfProtection, function(req, res, next) {
    res.redirect('/#/auth/login');
})

auth.post('/login', function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    User.getUserByUsername(username, function(err, user) {
        if (err)
            throw err;
        if (!user) {
            res.status(errors.user.error.notFound);
            return res.json({
                suc: false,
                msg: 'User not found'
            });
        }

        User.comparePassword(password, user.password, function(err, isMatch) {
            if (err)
                throw err;
            if (isMatch) {
                var now = Date.now();

                var payload = {
                    iss: "https://appstree.ovh/",
                    sid: user._doc._id,
                    usr: user._doc.firstname + ' ' + user._doc.lastname,
                    act: user._doc.isActive,
                    adm: user._doc.isAdmin,
                    jti: uuid(),
                    iat: now,
                    exp: now + parseInt(config.application.expiration),
                };

                var token = jwt.sign(payload, config.application.secret);

                res.status(errors.user.success.found);
                res.json({
                    suc: true,
                    tok: token,
                    err: null
                });
            } else {
                res.status(errors.user.error.wrongPassword);
                return res.json({
                    suc: false,
                    msg: 'Wrong password'
                });
            }
        });
    });
});

module.exports = auth;