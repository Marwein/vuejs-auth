var cluster = require('cluster');
if (cluster.isMaster) {
    var cpuCount = require('os').cpus().length;
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
} else {
    var express = require('express');
    var app = express();
    var ejs = require('ejs');
    var path = require('path');
    var cors = require('cors');
    var morgan = require('morgan');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var methodOverride = require('method-override');
    var session = require('express-session');
    var MongoStore = require('connect-mongo')(session);
    var mongoose = require('mongoose');
    var dotenv = require('dotenv');
    var config = require('./config');
    var users = require('./routes/users');
    var auth = require('./routes/auth');
    var appli = require('./routes/app');
    var hidePoweredBy = require('hide-powered-by');
    var helmet = require('helmet');

    var port = config.application.port;
    dotenv.load();

    // Connect To Database
    mongoose.connect(config.database, {
        useMongoClient: true
    });

    // On Connection
    mongoose.connection.on('connected', function() {
        console.log(`Connected to database ${config.database}`);
    });

    // On Error
    mongoose.connection.on('error', function(err) {
        console.log(`Database error: ${err}`);
    });

    app.use(hidePoweredBy({
        setTo: 'Bouallous Marwein'
    }));

    app.use(function(err, req, res, next) {
        if (err.code !== 'EBADCSRFTOKEN')
            return next(err)

        // handle CSRF token errors here
        var name = 'submit';
        var notice = 'Invalid csrf token , please refresh';
        helps.resJsonError(req, res, name, notice);
    });

    app.use(session({
        secret: config.application.secret,
        proxy: true,
        resave: true,
        saveUninitialized: true,
        /*
        store: new MongoStore({
            url: config.database,
        })
        */
    }));

    app.use(cors());
    app.use(helmet());
    app.use(morgan('dev'));

    // Create a new Express application
    app.engine('html', ejs.renderFile);
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({
        extended: false,
    }));
    app.use(bodyParser.json());
    app.use(methodOverride('X-HTTP-Method-Override'));
    app.use(express.static(path.join(__dirname, '../public/')));

    app.use('/', appli);
    app.use('/user', users);
    app.use('/auth', auth);

    app.set({
        'views': path.join(__dirname, '../public/'),
        'view engine': 'html'
    });

    app.get('/', function(req, res) {
        res.sendFile('index.html');
    });

    app.listen(port, function(worker) {
        var host = (process.env.APP_MODE === 'prod') ? `${process.env.APP_PRO}` : `${process.env.APP_DEV}:${port}`
        console.log(`[Processeur ${cluster.worker.id}]: Application executée en mode ${process.env.APP_MODE}, port ${port} => ${host}`);
    });
}

cluster.on('exit', function(worker) {
    console.log(`Worker ${worker.id} died :( it will restart`);
    cluster.fork();
});