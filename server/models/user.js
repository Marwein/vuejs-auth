var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var config = require('../config/index');
var uuid = require('uuid/v4');

// User Schema
var UserSchema = mongoose.Schema({
    uuid: {
        type: String
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    email: {
        type: String,
        required: true,
        index: {
            unique: true
        },
        email: true
    },
    username: {
        type: String,
        required: true,
        index: {
            unique: true
        },
    },
    password: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: false
    },
    salt: {
        type: String,
        default: ''
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback) {
    User.findById(id, callback);
};

module.exports.getUserByUsername = function(username, callback) {
    var query = { username: username };
    User.findOne(query, callback);
};

module.exports.getUserBy = function(key, value, callback) {
    var query = { key: value };
    User.findOne(query, callback);
};

module.exports.create = function(newUser, callback) {
    var hash = bcrypt.hashSync(newUser.password, parseInt(config.application.saltRounds));
    newUser.uuid = uuid();
    newUser.password = hash;
    newUser.save(callback);
};

module.exports.comparePassword = function(candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    });
};