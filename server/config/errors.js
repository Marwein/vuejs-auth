//Exports configuration
module.exports = {
    user: {
        error: {
            notFound: 420,
            wrongPassword: 421,
            notActive: 422,
            notAuthorized: 423,
            failedToRegister: 424
        },
        success: {
            found: 220,
            registered: 221,
            reset: 222,
        }
    }
}