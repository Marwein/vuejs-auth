const dotenv = require('dotenv')
dotenv.load()

//APPLICATION
const secret = process.env.APP_SECRET || 'supersecret'
const port = process.env.APP_PORT || 3000
const saltRounds = process.env.APP_SALT || 10
const expire = process.env.APP_EXP || 86400

//DATABASE
const db_type = process.env.DB_TYPE
const db_host = process.env.DB_HOST
const db_port = process.env.DB_PORT
const db_base = process.env.DB_BASE
const db_user = process.env.DB_USER
const db_pass = process.env.DB_PASS

//MAIL
const mail_host = process.env.MAIL_HOST
const mail_secu = process.env.MAIL_SECU
const mail_port = process.env.MAIL_PORT
const mail_user = process.env.MAIL_USER
const mail_pass = process.env.MAIL_PASS


//Exports configuration
module.exports = {
    database: `${db_type}://${db_user}:${db_pass}@${db_host}:${db_port}/${db_base}`,
    application: {
        secret: secret,
        port: port,
        saltRounds: saltRounds,
        expiration: expire,
    },
    mail: {
        host: mail_host,
        secure: mail_secu,
        port: mail_port,
        user: mail_user,
        pass: mail_pass
    }
}