const nodemailer = require('nodemailer')
const config = require('./config')
const transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: config.mail.secure, // secure:true for port 465, secure:false for port 587
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    }
})

module.exports = transporter